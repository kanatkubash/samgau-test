## Инструкция

### Frontend

 - Зайти в папку `frontend`
 - Установить зависимости `npm install`
 - Скомипилировать React приложение `npm run build`

### Backend

 - Сделать NuGet Restore для установки зависимости
 - В PM консоли запустить миграцию `Update-Database`
 - Запустить
 - Сайт должень запуститься по адресу `http://localhost:59821`. Если адрес другой зайти в настройки проекта -> Web -> Project Url = `http://localhost:59821` и Create Virtual Directory