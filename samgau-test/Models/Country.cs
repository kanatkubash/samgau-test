﻿using System.ComponentModel.DataAnnotations;

namespace samgau_test.Models
{
  public class Country
  {
    public Country() { }
    public Country(string name = null) => Name = name;

    [Key]
    public int ID { get; set; }
    public string Name { get; set; }
  }
}
