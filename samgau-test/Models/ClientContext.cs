﻿namespace samgau_test.Models
{
  using System.Data.Entity;

  public class ClientContext : DbContext
  {
    // Your context has been configured to use a 'ClientContext' connection string from your application's 
    // configuration file (App.config or Web.config). By default, this connection string targets the 
    // 'samgau_test.Models.ClientContext' database on your LocalDb instance. 
    // 
    // If you wish to target a different database and/or database provider, modify the 'ClientContext' 
    // connection string in the application configuration file.
    public ClientContext()
        : base("name=ClientContext")
    {
      Configuration.AutoDetectChangesEnabled = true;
    }

    // Add a DbSet for each entity type that you want to include in your model. For more information 
    // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

    public virtual DbSet<Client> Entities { get; set; }
    public virtual DbSet<Country> Countries { get; set; }
  }

  //public class MyEntity
  //{
  //    public int Id { get; set; }
  //    public string Name { get; set; }
  //}
}
