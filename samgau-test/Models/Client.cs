﻿using Newtonsoft.Json;
using samgau_test.Validations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace samgau_test.Models
{
  public class Client
  {
    [Key]
    public int ID { get; set; }
    [RequiredIfResident, CorrectIin]
    public string Iin { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public string Surname { get; set; }
    public string FatherName { get; set; }
    [ForeignKey("Country")]
    public int CountryId { get; set; }
    [JsonIgnore]
    public virtual Country Country { get; set; }
  }
}
