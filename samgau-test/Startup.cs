﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;

[assembly: OwinStartup(typeof(samgau_test.Startup))]

namespace samgau_test
{
  public class Startup
  {
    public object OwinEnvironment { get; private set; }

    public void Configuration(IAppBuilder app)
    {
      var physicalFileSystem = new PhysicalFileSystem("./Web");
      var options = new FileServerOptions
      {
        EnableDefaultFiles = true,
        FileSystem = physicalFileSystem
      };
      options.StaticFileOptions.FileSystem = physicalFileSystem;
      options.StaticFileOptions.ServeUnknownFileTypes = true;
      options.DefaultFilesOptions.DefaultFileNames = new[] { "index.html" };
      app.UseFileServer(options);
    }
  }
}
