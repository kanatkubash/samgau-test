﻿using samgau_test.Models;
using samgau_test.Repository;
using System.Threading.Tasks;
using System.Web.Http;

namespace samgau_test.Controllers
{
  [RoutePrefix("clients")]
  public class ClientController : ApiController
  {
    private readonly IRepository<Client> clientRepository;

    public ClientController(IRepository<Client> clientRepository) =>
      this.clientRepository = clientRepository;

    [Route("")]
    public async Task<IHttpActionResult> GetClients()
    {
      return Ok(await clientRepository.GetAll());
    }

    [Route("{id}")]
    public async Task<IHttpActionResult> Get(int id)
    {
      var client = await clientRepository.Get(id);
      if (client != null)
        return Ok(client);
      else
        return NotFound();
    }

    [Route(""), HttpPost]
    public async Task<IHttpActionResult> Save([FromBody]Client client)
    {
      if (client == null)
        return BadRequest();
      if (ModelState.IsValid)
        return Ok(await clientRepository.Save(client));
      else
        return BadRequest(ModelState);
    }

    [Route(""), HttpPut]
    public async Task<IHttpActionResult> Update([FromBody]Client client)
    {
      if (client == null)
        return BadRequest();
      if (ModelState.IsValid)
        return Ok(await clientRepository.Update(client));
      else
        return BadRequest(ModelState);
    }

    [Route("{id}"), HttpDelete]
    public async Task<IHttpActionResult> Delete(int id)
    {
      if (await clientRepository.Delete(id))
        return Ok();
      else
        return NotFound();
    }
  }
}
