﻿using samgau_test.Models;
using samgau_test.Repository;
using System.Threading.Tasks;
using System.Web.Http;

namespace samgau_test.Controllers
{
  [RoutePrefix("country")]
  public class CountryController : ApiController
  {
    private readonly IRepository<Country> countryRepository;
    public CountryController(IRepository<Country> countryRepository) =>
      this.countryRepository = countryRepository;

    [Route("")]
    public async Task<IHttpActionResult> GetAll()
    {

      var countries = await countryRepository.GetAll();
      return Ok(countries);
    }
  }
}
