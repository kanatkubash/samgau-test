﻿using samgau_test.Helpers;
using samgau_test.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace samgau_test.Repository
{
  public class DbClientRepository : IRepository<Client>
  {
    protected readonly ClientContext db;

    public DbClientRepository(ClientContext db) => this.db = db;

    public Task<Client> Get(params object[] keys)
    {
      return db.Entities.FindAsync(keys);
    }

    public async Task<IEnumerable<Client>> GetAll()
    {
      return await db.Entities.ToListAsync();
    }

    public async Task<Client> Save(Client item)
    {
      db.Entities.Add(item);
      await db.SaveChangesAsync();
      return item;
    }

    public async Task<Client> Update(Client item)
    {
      var client = await db.Entities.FindAsync(item.ID);

      if (client == null)
        return null;

      db.Entry(client).CurrentValues.SetValues(item);
      await db.SaveChangesAsync();
      return client;
    }

    public async Task<bool> Delete(params object[] keys)
    {
      var toRemove = new Client() { ID = (int)keys[0] };
      db.Entities.Attach(toRemove);
      db.Entities.Remove(toRemove);
      return await new TryCatch<DbUpdateConcurrencyException, int>()
        .Invoke(db.SaveChangesAsync) == 1;
    }
  }
}
