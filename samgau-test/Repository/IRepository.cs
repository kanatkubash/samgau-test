﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace samgau_test.Repository
{
  public interface IRepository<T>
  {
    Task<IEnumerable<T>> GetAll();
    Task<T> Get(params object[] keys);
    Task<T> Save(T item);
    Task<T> Update(T item);
    Task<bool> Delete(params object[] keys);
  }
}
