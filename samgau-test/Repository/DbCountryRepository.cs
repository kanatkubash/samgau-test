﻿using samgau_test.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace samgau_test.Repository
{
  public class DbCountryRepository : IRepository<Country>
  {
    protected readonly DbSet<Country> dbSet;
    public DbCountryRepository(DbSet<Country> dbSet) => this.dbSet = dbSet;

    public Task<bool> Delete(params object[] keys)
    {
      throw new System.NotImplementedException();
    }

    public Task<Country> Get(params object[] keys)
    {
      throw new System.NotImplementedException();
    }

    public async Task<IEnumerable<Country>> GetAll()
    {
      return await dbSet.ToListAsync();
    }

    public IEnumerable<Country> s()
    {
      return this.dbSet.ToList();
    }

    public Task<Country> Save(Country item)
    {
      throw new System.NotImplementedException();
    }

    public Task<Country> Update(Country item)
    {
      throw new System.NotImplementedException();
    }
  }
}
