import Client from '../models/Client';
import axios from 'axios';
import { ENDPOINT } from './config';

/// local cache like stuff
var clients: Client[];

export const saveClient = async (client: Client) => {
  var isNew = client.id == null;
  var request = isNew ? axios.post : axios.put;
  var { data } = await request(`${ENDPOINT}/clients`, client);

  var updatedClient = new Client(data);

  if (typeof clients == 'undefined') return;

  if (isNew) clients.push(updatedClient);
  else {
    var index = clients.findIndex(({ id }) => id == client.id);
    index >= 0 && clients.splice(index, 1, updatedClient);
  }
};

export const getClient = async (clientId: number) => {
  var index = clients ? clients.findIndex(({ id }) => id == clientId) : -1;

  if (index > -1) return clients[index];

  var { data } = await axios.get(`${ENDPOINT}/clients/${clientId}`);
  return new Client(data);
};

export const getAll = async () => {
  if (typeof clients == 'undefined') {
    var { data } = await axios.get(`${ENDPOINT}/clients`);
    var rawClients: Client[] = data;

    clients = rawClients.map(raw => new Client(raw));
  }

  return clients;
};

export const deleteClient = async (clientId: number) => {
  await axios.delete(`${ENDPOINT}/clients/${clientId}`);

  if (typeof clients == 'undefined') return;

  var index = clients.findIndex(({ id }) => id == clientId);
  clients.splice(index, 1);
};
