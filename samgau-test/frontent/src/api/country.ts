import Country from '../models/Country';
import axios from 'axios';
import { ENDPOINT } from './config';

export const getCountries = async (): Promise<Country[]> => {
  var { data } = await axios.get(`${ENDPOINT}/country`);
  return data;
};
