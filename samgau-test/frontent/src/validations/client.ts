import Client from '../models/Client';
import validate from 'validate.js';

const COUNTRY_KZ = 1;

validate.validators.iinConditional = function(
  value: string,
  options: any,
  key: string,
  attributes: Client
) {
  if (attributes.countryId == COUNTRY_KZ && (value || '').length != 12)
    return '^ИИН должен содержать 12 цифр';
};

const clientConstraint = {
  name: {
    presence: true,
    format: {
      pattern: '[a-zа-я]+',
      flags: 'i',
      message: '^Имя должно содержать только буквы'
    }
  },
  surname: {
    presence: true,
    format: {
      pattern: '[a-zа-я]+',
      flags: 'i',
      message: '^Фамилия должна содержать только буквы'
    }
  },
  iin: {
    iinConditional: true
  },
  countryId: {
    presence: true,
    numericality: { notEqualTo: 0 }
  }
};

export const canSaveClient = (client: Client) => {
  return validate(client, clientConstraint);
};
