export default class Client {
  id?: number;
  iin?: string;
  name!: string;
  surname!: string;
  fatherName?: string;
  countryId!: number;

  constructor(args?: Partial<Client>) {
    args && Object.assign(this, args);
  }
}
