import { CatalogItem } from '../components/Select';

export default class Country implements CatalogItem {
  id!: number;
  name!: string;
}
