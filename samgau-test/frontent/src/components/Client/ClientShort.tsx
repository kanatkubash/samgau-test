import React from 'react';
import ClientModel from '../../models/Client';
import Country from '../../models/Country';
import styled from 'styled-components';

const Bordered = styled.div`
   {
    padding: 5px;
    transition: background 0.3s;
    &:not(:last-child) {
      border-bottom: 1px solid #eee;
    }
    &:hover {
      background: #f2f2f2;
    }
  }
`;

interface ClientProps {
  client: ClientModel;
}

export default class Client extends React.Component<ClientProps> {
  render() {
    const { children } = this.props;
    const { client } = this.props;

    return (
      <Bordered>
        <div className='columns'>
          <div className='column is-one-fifth'>{client.surname}</div>
          <div className='column is-one-fifth'>{client.name}</div>
          <div className='column is-one-fifth'>{client.fatherName}</div>
          <div className='column is-one-fifth'>{client.iin}</div>
          <div className='column is-one-fifth'>{children}</div>
        </div>
      </Bordered>
    );
  }
}
