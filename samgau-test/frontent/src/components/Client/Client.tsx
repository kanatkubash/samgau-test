import React from 'react';
import Input from '../Input';
import ClientModel from '../../models/Client';
import Select from '../Select';
import Country from '../../models/Country';

interface ClientProps {
  title: string;
  client: ClientModel;
  update(client: ClientModel): void;
  errors?: { [a in keyof ClientModel]: string };
  countries: Country[];
  width?: string;
}

export default class Client extends React.Component<ClientProps> {
  constructor(props: ClientProps) {
    super(props);
  }

  setValue = (key: keyof ClientModel, value: string | number) => {
    this.props.update &&
      this.props.update({ ...this.props.client, [key]: value });
  };

  render() {
    return (
      <div style={{ width: this.props.width }}>
        <h1 className='title is-4'>{this.props.title}</h1>
        <div className='columns is-multiline'>
          <Input
            label='Фамилия'
            value={this.props.client.surname}
            type='text'
            className='column is-half'
            onChange={value => this.setValue('surname', value)}
            errorMessage={this.props.errors && this.props.errors['surname']}
          />
          <Input
            label='Имя'
            value={this.props.client.name}
            type='text'
            className='column is-half'
            onChange={value => this.setValue('name', value)}
            errorMessage={this.props.errors && this.props.errors['name']}
          />
          <Input
            label='Отчество'
            value={this.props.client.fatherName}
            type='text'
            className='column is-half'
            onChange={value => this.setValue('fatherName', value)}
            errorMessage={this.props.errors && this.props.errors['fatherName']}
          />
          <Input
            label='ИИН'
            value={this.props.client.iin}
            type='number'
            className='column is-half'
            onChange={value => this.setValue('iin', value)}
            errorMessage={this.props.errors && this.props.errors['iin']}
          />
          <Select
            label='Страна'
            className='column is-half'
            errorMessage={this.props.errors && this.props.errors['countryId']}
            onChange={value => this.setValue('countryId', value)}
            value={this.props.client.countryId}
            values={this.props.countries}
          />
        </div>
      </div>
    );
  }
}
