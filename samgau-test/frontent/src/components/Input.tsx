import React, { ChangeEvent } from 'react';

export interface InputProp {
  errorMessage?: string;
  type: 'number' | 'text';
  className?: string;
  value?: string | number;
  onChange: (value: string) => void;
  label: string;
}

export default class extends React.Component<InputProp> {
  handleChange = (evt: ChangeEvent<HTMLInputElement>) => {
    evt.preventDefault();
    var value = evt.target.value;
    if (this.props.type == 'number')
      this.props.onChange(value.replace(/[^\d]/, ''));
    else this.props.onChange(value);
  };

  render() {
    return (
      <div className={this.props.className}>
        <label className='label'>{this.props.label}</label>
        <input
          className={`input ${this.props.errorMessage ? 'is-danger' : ''}`}
          type='text'
          onChange={this.handleChange}
          value={this.props.value || ''}
        />
        {this.props.errorMessage || <br />}
        <div />
      </div>
    );
  }
}
