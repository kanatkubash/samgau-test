import React, { ChangeEvent } from 'react';

type StrOrNum = string | number;

export interface CatalogItem {
  id: number;
  name: any;
}

export interface SelectProp<T> {
  errorMessage?: string;
  className?: string;
  value?: StrOrNum;
  values: T[];
  onChange: (value: StrOrNum) => void;
  label: string;
}

export default class<T extends CatalogItem> extends React.Component<
  SelectProp<T>
> {
  handleChange = (evt: ChangeEvent<HTMLSelectElement>) => {
    evt.preventDefault();
    var value: any = evt.target.value;
    this.props.onChange(value);
  };

  render() {
    return (
      <div className={this.props.className}>
        <label className='label'>{this.props.label}</label>
        <div
          className={`select ${this.props.errorMessage ? 'is-danger' : ''}`}
          style={{ display: 'block' }}
        >
          <select
            className='input'
            onChange={this.handleChange}
            value={this.props.value || '-1'}
          >
            <option disabled value='-1' />
            {this.props.values.map(({ id, name }) => (
              <option key={id} value={id}>
                {name}
              </option>
            ))}
          </select>
        </div>
        {this.props.errorMessage || <br />}
        <div />
      </div>
    );
  }
}
