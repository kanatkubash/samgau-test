var mutexStorage = new Map();

export function mutex(target: any, key: string, desc: PropertyDescriptor) {
  var originalFunction = desc.value;
  desc.value = async function() {
    if (mutexStorage.has(this)) return;
    mutexStorage.set(this, true);
    //@ts-ignore
    await originalFunction.apply(this, arguments);
    mutexStorage.delete(this);
  };
}
