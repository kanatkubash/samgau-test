import React, { Component } from 'react';
import { Route, HashRouter } from 'react-router-dom';
import 'bulma/css/bulma.css';
import './App.css';
import DefaultView from './views/DefaultView';
import AddClientView from './views/AddClientView';
import ClientListView from './views/ClientListView';
import ClientView from './views/ClientView';

class App extends Component {
  constructor(a: { q: number }) {
    super(a);
  }

  render() {
    return (
      <div>
        <HashRouter>
          <Route component={DefaultView} path='/' exact />
          <Route component={AddClientView} path='/add' />
          <Route component={ClientListView} path='/clients' exact />
          <Route component={ClientView} path='/clients/:clientId' />
        </HashRouter>
      </div>
    );
  }
}

export default App;
