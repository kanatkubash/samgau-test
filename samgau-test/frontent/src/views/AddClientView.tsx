import React, { Component } from 'react';
import Centered from '../components/Centered';
import Client from '../components/Client/Client';
import ClientModel from '../models/Client';
import { Link, Redirect } from 'react-router-dom';
import Country from '../models/Country';
import { getCountries } from '../api/country';
import { saveClient } from '../api/client';
import { mutex } from '../decorators';
import { canSaveClient } from '../validations/client';

export default class AddClientView extends Component<
  { client?: ClientModel },
  {
    client: ClientModel;
    countries: Country[];
    saving: boolean;
    errors?: any;
    goToClientList: boolean;
  }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      client: this.props.client || new ClientModel(),
      countries: [],
      saving: false,
      errors: null,
      goToClientList: false
    };
    getCountries().then(countries => this.setState({ countries }));
    this.save = this.save.bind(this);
  }
  update = (client: ClientModel) => {
    this.setState({ client });
  };
  @mutex
  async save() {
    try {
      var errors = canSaveClient(this.state.client);
      this.setState({ errors });
      if (!errors) {
        await saveClient(this.state.client);
        this.setState({ goToClientList: true });
      }
    } catch (e) {
      if (e.response.status == 400) {
        var { data } = e.response;
        this.setState({ errors: data.modelState });
      }
    }
  }

  render() {
    if (this.state.goToClientList) return <Redirect to='/clients' />;

    return (
      <Centered>
        <div>
          <Client
            width='80vw'
            title={
              this.state.client.id
                ? 'Редактирование клиента'
                : 'Добавить клиента'
            }
            client={this.state.client}
            update={this.update}
            countries={this.state.countries}
            errors={this.state.errors}
          />
          <a
            className='button is-primary'
            onClick={this.save}
            style={{ marginRight: '20px' }}
          >
            Сохранить
          </a>
          <Link className='button' to='/'>
            Отмена
          </Link>
        </div>
      </Centered>
    );
  }
}
