import React from 'react';
import { Link } from 'react-router-dom';
import Centered from '../components/Centered';
import styled from 'styled-components';

const LocalCentered = styled(Centered)`
  a:first-child {
    margin-bottom: 20px;
  }
`;

export default class DefaultView extends React.Component {
  render() {
    return (
      <LocalCentered>
        <div>
          <Link to='/add' className='button is-primary is-block'>
            Add clients
          </Link>
          <Link to='/clients' className='button is-block'>
            Show clients
          </Link>
        </div>
      </LocalCentered>
    );
  }
}
