import React, { Component } from 'react';
import AddClientView from './AddClientView';
import { getClient } from '../api/client';
import Client from '../models/Client';

export default class ClientView extends Component<{}, { client?: Client }> {
  constructor(props: any) {
    super(props);
    this.state = { client: undefined };
    //@ts-ignore
    getClient(this.props.match.params.clientId).then(client =>
      this.setState({ client })
    );
  }

  render() {
    return this.state.client ? (
      <AddClientView client={this.state.client} />
    ) : null;
  }
}
