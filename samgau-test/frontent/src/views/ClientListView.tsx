import React, { Component } from 'react';
import Centered from '../components/Centered';
import ClientShort from '../components/Client/ClientShort';
import Client from '../models/Client';
import Country from '../models/Country';
import { getCountries } from '../api/country';
import { getAll, deleteClient } from '../api/client';
import groupBy from 'lodash.groupby';
import map from 'lodash.map';
import keyBy from 'lodash.keyby';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { mutex } from '../decorators';

export default class ClientListView extends Component<
  {},
  { clients: Client[]; countries: Country[] }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      clients: [],
      countries: []
    };
    getCountries().then(countries => this.setState({ countries }));
    getAll().then(clients => this.setState({ clients }));
  }

  get clientsGrouped() {
    return groupBy(this.state.clients, 'countryId');
  }
  get countriesGrouped() {
    return keyBy(this.state.countries, 'id');
  }
  @mutex
  async delete(clientId: number) {
    deleteClient(clientId).then(() =>
      getAll().then(clients => this.setState({ clients }))
    );
  }

  render() {
    if (Object.keys(this.countriesGrouped).length == 0) return <div />;

    var clientsGroupedByCountry = map(
      this.clientsGrouped,
      (clients, countryId) => {
        return (
          <div key={countryId}>
            <div>
              <h4 className='title is-6' style={{ marginBottom: 0 }}>
                {this.countriesGrouped[countryId].name}
              </h4>
              <hr style={{ margin: 0 }} />
            </div>
            {clients.map(client => (
              <ClientShort key={client.id} client={client}>
                <div>
                  <Link to={`/clients/${client.id}`}>
                    <span
                      className='icon has-text-info'
                      style={{ marginRight: '10px' }}
                    >
                      <i className='fas  fa-lg fa-user-edit' />
                    </span>
                  </Link>
                  <a onClick={() => this.delete(client.id!)}>
                    <span className='icon has-text-danger'>
                      <i className='fas  fa-lg fa-user-minus' />
                    </span>
                  </a>
                </div>
              </ClientShort>
            ))}
          </div>
        );
      }
    );

    return (
      <Centered>
        <div style={{ minWidth: '70vw' }}>
          <h1 className='title is-4'>Список клиентов</h1>
          {clientsGroupedByCountry}
        </div>
      </Centered>
    );
  }
}
