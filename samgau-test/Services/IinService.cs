﻿using samgau_test.Helpers;
using System;

namespace samgau_test.Services
{
  using Cu = System.Globalization.CultureInfo;
  using Dts = System.Globalization.DateTimeStyles;

  public interface IIinService
  {
    bool IsValid(string iin);
    IinParseInfo Parse(string iin);
  }
  public class IinService : IIinService
  {
    public bool IsValid(string iin)
    {
      if (iin.Length != 12)
        return false;

      var lastDigit = int.Parse(iin[11].ToString());
      var sum = this.Sum(iin, 0) % 11;
      if (sum == 10)
        sum = this.Sum(iin, 2) % 11;
      ///means natural person
      if (iin[4] <= '3' && this.ParseBirthday(iin) == null)
        return false;

      return sum != 10 && sum == lastDigit;
    }

    public IinParseInfo Parse(string iin)
    {
      if (!this.IsValid(iin))
        return null;

      if (iin[4] > '3')
        return new IinParseInfo() { NaturalPersonBool = 0, Iin = iin };

      var bday = this.ParseBirthday(iin).Value;
      return new IinParseInfo()
      {
        Birthday = bday,
        Age = DateTime.Now.DiffInYears(bday),
        Iin = iin,
        NaturalPersonBool = 1,
      };
    }

    public DateTime? ParseBirthday(string iin)
    {
      var date = this.GetCentury(iin[6]) + iin.Substring(0, 6);
      if (DateTime.TryParseExact(date, "yyyyMMdd", Cu.InvariantCulture, Dts.None, out DateTime dt))
        return dt;
      else
        return null;
    }

    private int GetCentury(char digit7)
    {
      var factor = Math.Ceiling(int.Parse(digit7.ToString()) / 2.0);
      return (int)(17 + factor);
    }

    private int Sum(string iin, int weight)
    {
      if (weight != 0 && weight != 2)
        throw new InvalidOperationException("Weight should be 0 or 2");

      var sum = 0;
      /// weight becomes 1... or 3... after (weight % 11) + 1
      for (var i = 0; i < 11; i++, weight++)
        sum += ((weight % 11) + 1) * int.Parse(iin[i].ToString());
      return sum;
    }
  }
  public sealed class IinParseInfo
  {
    public string Iin { get; set; }
    public int Age { get; set; }
    public DateTime Birthday { get; set; }
    public int NaturalPersonBool { get; set; }
  }
}
