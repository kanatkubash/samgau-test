﻿namespace samgau_test.Migrations
{
  using System.Data.Entity.Migrations;

  public partial class AlterClientsChangeSomeFieldsToRequired : DbMigration
  {
    public override void Up()
    {
      AlterColumn("dbo.Clients", "Name", c => c.String(nullable: false));
      AlterColumn("dbo.Clients", "Surname", c => c.String(nullable: false));
    }

    public override void Down()
    {
      AlterColumn("dbo.Clients", "Surname", c => c.String());
      AlterColumn("dbo.Clients", "Name", c => c.String());
    }
  }
}
