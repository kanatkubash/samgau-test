﻿namespace samgau_test.Migrations
{
  using System.Data.Entity.Migrations;

  public partial class AddForeignKeys : DbMigration
  {
    public override void Up()
    {
      AddColumn("dbo.Clients", "CountryId", c => c.Int(nullable: false));
      CreateIndex("dbo.Clients", "CountryId");
      AddForeignKey("dbo.Clients", "CountryId", "dbo.Countries", "ID", cascadeDelete: false);
      DropColumn("dbo.Clients", "Country");
    }

    public override void Down()
    {
      AddColumn("dbo.Clients", "Country", c => c.Int(nullable: false));
      DropForeignKey("dbo.Clients", "CountryId", "dbo.Countries");
      DropIndex("dbo.Clients", new[] { "CountryId" });
      DropColumn("dbo.Clients", "CountryId");
    }
  }
}
