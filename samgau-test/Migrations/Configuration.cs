﻿namespace samgau_test.Migrations
{
  using samgau_test.Models;
  using System.Data.Entity.Migrations;
  using System.Linq;

  internal sealed class Configuration : DbMigrationsConfiguration<samgau_test.Models.ClientContext>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(samgau_test.Models.ClientContext context)
    {
      SeedCountries(context);
    }

    private void SeedCountries(samgau_test.Models.ClientContext context)
    {
      if (context.Countries.Count() == 0)
      {
        var countries = new[] { "Казахстан", "Россия", "Турция", "Украина", "Белоруссия", };
        context.Countries.AddRange(from c in countries select new Country(c));
      }
    }
  }
}
