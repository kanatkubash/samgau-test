﻿namespace samgau_test.Migrations
{
  using System.Data.Entity.Migrations;

  public partial class CreateClient : DbMigration
  {
    public override void Up()
    {
      CreateTable(
          "dbo.Countries",
          c => new
          {
            ID = c.Int(nullable: false, identity: true),
            Name = c.String(),
          })
          .PrimaryKey(t => t.ID);

      CreateTable(
          "dbo.Clients",
          c => new
          {
            ID = c.Int(nullable: false, identity: true),
            Iin = c.String(maxLength: 12, nullable: true),
            Name = c.String(),
            Surname = c.String(),
            FatherName = c.String(nullable: true),
            Country = c.Int(nullable: false),
          })
          .PrimaryKey(t => t.ID);

    }

    public override void Down()
    {
      DropTable("dbo.Clients");
      DropTable("dbo.Countries");
    }
  }
}
