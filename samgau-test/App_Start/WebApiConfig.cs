﻿using Newtonsoft.Json.Serialization;
using samgau_test.App_Start;
using samgau_test.Validations;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Validation;

namespace samgau_test
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services
      config.MapHttpAttributeRoutes();
      config.EnableCors();
      // Web API routes      
      GlobalConfiguration.Configuration.Services.Replace(
        typeof(IHttpControllerActivator),
        new CustomControllerActivator()
        );

      var defaultImpl = GlobalConfiguration.Configuration.Services.GetBodyModelValidator();
      GlobalConfiguration.Configuration.Services.Replace(
        typeof(IBodyModelValidator),
        new PrefixlessBodyModelValidator(defaultImpl));

      var camelCaseJson = new CamelCasePropertyNamesContractResolver();
      config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = camelCaseJson;

    }
  }

}
