﻿using samgau_test.Controllers;
using samgau_test.Models;
using samgau_test.Repository;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace samgau_test.App_Start
{
  /// <summary>
  /// Very simple controller injection is going on here
  /// </summary>
  public class CustomControllerActivator : IHttpControllerActivator
  {
    public readonly ClientContext db;
    public CustomControllerActivator()
    {
      this.db = new ClientContext();
    }

    public IHttpController Create(
      HttpRequestMessage request,
      HttpControllerDescriptor controllerDescriptor,
      Type controllerType
      )
    {
      if (controllerType == typeof(CountryController))
        return new CountryController(new DbCountryRepository(db.Countries));
      if (controllerType == typeof(ClientController))
        return new ClientController(new DbClientRepository(new ClientContext()));
      return null;
    }
  }
}
