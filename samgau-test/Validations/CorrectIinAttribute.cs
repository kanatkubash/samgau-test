﻿using samgau_test.Services;
using System.ComponentModel.DataAnnotations;

namespace samgau_test.Validations
{
  public class CorrectIinAttribute : ValidationAttribute
  {
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
      if (string.IsNullOrEmpty((string)value))
        return ValidationResult.Success;

      var iinService = new IinService();
      if (iinService.IsValid((string)value))
        return ValidationResult.Success;
      else
        return new ValidationResult("Invalid Iin");
    }
  }
}
