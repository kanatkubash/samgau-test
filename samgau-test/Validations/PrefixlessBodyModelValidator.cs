﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;
using System.Web.Http.Validation;

namespace samgau_test.Validations
{
  public class PrefixlessBodyModelValidator : IBodyModelValidator
  {
    readonly IBodyModelValidator defaultImpl;
    public PrefixlessBodyModelValidator(IBodyModelValidator defaultImpl) => this.defaultImpl = defaultImpl;
    public bool Validate(
      object model,
      Type type,
      ModelMetadataProvider metadataProvider,
      HttpActionContext actionContext,
      string keyPrefix)
    {
      return defaultImpl.Validate(model, type, metadataProvider, actionContext, "");
    }
  }
}
