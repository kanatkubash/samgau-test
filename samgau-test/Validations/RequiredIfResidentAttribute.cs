﻿using samgau_test.Models;
using System.ComponentModel.DataAnnotations;

namespace samgau_test.Validations
{
  public class RequiredIfResidentAttribute : ValidationAttribute
  {
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
      var client = (Client)validationContext.ObjectInstance;
      if (client.CountryId == 1 && string.IsNullOrEmpty((string)value))
        return new ValidationResult("iin is required");
      return ValidationResult.Success;
    }
  }
}
