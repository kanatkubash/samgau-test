﻿using System;
using System.Threading.Tasks;

namespace samgau_test.Helpers
{
  /// <summary>
  /// Functional way of doing try catch
  /// </summary>
  /// <typeparam name="E"></typeparam>
  /// <typeparam name="T"></typeparam>
  public class TryCatch<E, T> where E : Exception
  {
    /// <summary>
    /// Invokes given function and returns defaultvalue
    /// if error occured and catched exception is same as provided
    /// </summary>
    /// <param name="func"></param>
    /// <param name="defaultValue"></param>
    /// <returns></returns>
    public async Task<T> Invoke(Func<Task<T>> func, T defaultValue = default(T))
    {
      try
      {
        var asyncResult = func.BeginInvoke(null, null);
        return await await Task.Factory.FromAsync(asyncResult, func.EndInvoke);
      }
      catch (E e)
      {
        return defaultValue;
      }
    }

  }
}
