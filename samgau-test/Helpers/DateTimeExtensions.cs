﻿using System;

namespace samgau_test.Helpers
{
  public static class DateTimeExtensions
  {
    public static int DiffInYears(this DateTime start, DateTime end)
    {
      return Math.Abs((end.Year - start.Year - 1) +
        (((end.Month > start.Month) ||
        ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0));
    }
  }
}
